package com.bilas.task1;

public abstract class Fish implements Animal {

    int age;
    int weight;

    abstract void swim();
}
