package com.bilas.task1;

public class Eagle extends Bird {

    public Eagle(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Eagle{" +
                "age=" + age +
                ", weight=" + weight +
                '}';
    }

    boolean fly() {
        return true;
    }

    public void sleep() {
        System.out.println("I sleep on a tree");
    }

    public void eatAnything() {
        System.out.println("I eat little animals and fish");
    }

    public void about() {
        System.out.println("I am " + toString());
        sleep();
        eatAnything();
    }
}
