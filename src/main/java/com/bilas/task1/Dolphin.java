package com.bilas.task1;

public class Dolphin extends Fish {

    public Dolphin(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Dolphin{" +
                "age=" + age +
                ", weight=" + weight +
                '}';
    }

    void swim() {
        System.out.println("Swim");
    }

    public void sleep() {
        System.out.println("slipping");
    }

    public void eatAnything() {
        System.out.println("eating fish");
    }
}
