package com.bilas.task1;

public class Parrot extends Bird {

    public Parrot(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Parrot{" +
                "age=" + age +
                ", weight=" + weight +
                '}';
    }

    boolean fly() {
        return true;
    }

    public void sleep() {
        System.out.println("I sleep in Gate");
    }

    public void eatAnything() {
        System.out.println("i eat corn");
    }

    public void about() {
        System.out.println("I am " + toString());
        sleep();
        eatAnything();
    }
}
