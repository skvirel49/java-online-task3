package com.bilas.task1;

public class Shark extends Fish {

    public Shark(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Shark{" +
                "age=" + age +
                ", weight=" + weight +
                '}';
    }

    void swim() {
        System.out.println("swim");
    }

    public void sleep() {
        System.out.println("Sharks never slips");
    }

    public void eatAnything() {
        System.out.println("eating people");
    }
}
