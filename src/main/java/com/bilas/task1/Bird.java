package com.bilas.task1;

public abstract class Bird implements Animal {

    int age;
    int weight;

    abstract boolean fly();

}
