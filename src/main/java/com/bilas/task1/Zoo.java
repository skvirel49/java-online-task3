package com.bilas.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zoo {
    List<Animal> animals = new ArrayList<Animal>();

    public void addFishToZoo() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Add dolphin or shark: 1 or 2");
        int num;
        while (true) {
            num = scanner.nextInt();

            if (num == 1) {
                System.out.println("you choose dolphin");
                break;
            } else if (num == 2) {
                System.out.println("you choose shark");
                break;
            } else {
                System.out.println("try again");
            }
        }

        if (num == 1) {
            int age;
            do {
                System.out.println("add dolphin age:");
                age = scanner.nextInt();
            } while (age < 1);
            int weight;
            do {
                System.out.println("add dolphin weight:");
                weight = scanner.nextInt();
            } while (weight < 1);
            animals.add(new Dolphin(age, weight));
        }
        if (num == 2) {
            int age;
            do {
                System.out.println("add shark age:");
                age = scanner.nextInt();
            } while (age < 1);
            int weight;
            do {
                System.out.println("add shark weight:");
                weight = scanner.nextInt();
            } while (weight < 1);
            animals.add(new Shark(age, weight));
        }

    }

    public void addBirdToZoo() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Add eagle or parrot: 1 or 2");
        int num;
        while (true) {
            num = scanner.nextInt();

            if (num == 1) {
                System.out.println("you choose eagle");
                break;
            } else if (num == 2) {
                System.out.println("you choose parrot");
                break;
            } else {
                System.out.println("try again");
            }
        }

        if (num == 1) {
            int age;
            do {
                System.out.println("add eagle age:");
                age = scanner.nextInt();
            } while (age < 1);
            int weight;
            do {
                System.out.println("add eagle weight:");
                weight = scanner.nextInt();
            } while (weight < 1);
            animals.add(new Eagle(age, weight));
        }
        if (num == 2) {
            int age;
            do {
                System.out.println("add parrot age:");
                age = scanner.nextInt();
            } while (age < 1);
            int weight;
            do {
                System.out.println("add parrot weight:");
                weight = scanner.nextInt();
            } while (weight < 1);
            animals.add(new Parrot(age, weight));
        }
    }

    public void showAllAnimals() {
        for (Animal animal : animals) {
            System.out.println(animal.toString());
        }
    }

    public void showDolphins() {
        for (Animal animal : animals) {
            if (animal instanceof Dolphin) {
                System.out.println(animal.toString());
            }
        }
    }

    public void showSharks() {
        for (Animal animal : animals) {
            if (animal instanceof Shark) {
                System.out.println(animal.toString());
            }
        }
    }

    public void showParrots() {
        for (Animal animal : animals) {
            if (animal instanceof Parrot) {
                System.out.println(animal.toString());
            }
        }
    }

    public void showEagles() {
        for (Animal animal : animals) {
            if (animal instanceof Eagle) {
                System.out.println(animal);
            }
        }
    }
}
