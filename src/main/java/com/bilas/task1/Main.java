package com.bilas.task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Zoo zoo = new Zoo();
        boolean b = true;
        do {
            System.out.println("add animal to zoo:");
            System.out.println("to add fish add 1\n" +
                    "to add bird add 2 \n add 0 to exit");
            int x = scanner.nextInt();
            switch (x) {
                case 1:
                    zoo.addFishToZoo();
                    break;
                case 2:
                    zoo.addBirdToZoo();
                    break;
                case 0:
                    b = false;
            }

        } while (b);

        boolean boo = true;
        do {
            System.out.println("to show all animals add 1 \nto show dolphins add 2 \n" +
                    "to show sharks add 3 \nto show parrots add 4 \n" +
                    "to show eagles add 5 \n to exit add 0");
            int x = scanner.nextInt();
            switch (x) {
                case 1:
                    zoo.showAllAnimals();
                    break;
                case 2:
                    zoo.showDolphins();
                    break;
                case 3:
                    zoo.showSharks();
                    break;
                case 4:
                    zoo.showParrots();
                case 5:
                    zoo.showEagles();
                case 0:
                    boo = false;
            }
        } while (boo);

        System.out.println("thank you! goodbye!");
    }
}
