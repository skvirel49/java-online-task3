package com.bilas.task2;

import java.util.Scanner;

public class Triangle implements Shape {

    private int a;
    private int b;
    private int c;
    private String color;

    public Triangle(int a, int b, int c, String color) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.color = color;
    }

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    public void squareOfShape() {
        if (c == Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))) {
            System.out.println("square of triangle = " + ((a * b) / 2));
        } else {
            System.out.println("please help! I can't count it");
        }
    }

    public String colour() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("add color:");
        String color = scanner.nextLine();
        System.out.println("my color is : " + color);
        this.color = color;
        return color;
    }
}
