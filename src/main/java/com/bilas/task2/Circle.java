package com.bilas.task2;

import java.util.Scanner;

public class Circle implements Shape {

    private int radius;
    private String color;

    public Circle(int radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }

    public void squareOfShape() {
        System.out.println("Circle square = " + (Math.pow(radius, 2) * Math.PI));
    }

    public String colour() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("add color:");
        String color = scanner.nextLine();
        System.out.println("my color is : " + color);
        this.color = color;
        return color;
    }
}
