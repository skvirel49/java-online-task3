package com.bilas.task2;

import java.util.Scanner;

public class Rectangle implements Shape {

    private int a;
    private int b;
    private String color;

    public Rectangle(int a, int b, String color) {
        this.a = a;
        this.b = b;
        this.color = color;
    }

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void squareOfShape() {
        System.out.println("square of rectangle = " + (a * b));
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    public String colour() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("add color:");
        String color = scanner.nextLine();
        System.out.println("my color is : " + color);
        this.color = color;
        return color;
    }

}
